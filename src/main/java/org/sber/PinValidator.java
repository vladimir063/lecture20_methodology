package org.sber;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.sber.exception.AccountIsLockedException;
import org.sber.exception.PinValidatorException;

import java.time.LocalTime;

@Getter
@Setter
@RequiredArgsConstructor
public class PinValidator {

    private final String pin;
    private boolean isCorrect;
    private int count;
    private LocalTime time = LocalTime.now();
    private boolean isLocked;

    public void setPinCode(String pin) {
        if (isLocked) {
            throw new AccountIsLockedException("Вы три раза ввели неверный пин код. Аккаунт заблокирован до " + time.toString());
        }
        if (this.pin.equals(pin)) {
            isCorrect = true;
            return;
        }
        isCorrect = false;
        // делаем так , чтобы если три раза ввести неправильный пин аккаунт заблокировался
        count++;
        if (count == 3) {
            isLocked = true;
            count = 0;
            time = time.plusSeconds(5);
            new Thread(() -> {   // делаем так , чтобы через 5 секунд аккаунт разблокировался
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                isLocked = false;
            }).start();
        }
    }
}



