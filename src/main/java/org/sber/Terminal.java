package org.sber;

public interface Terminal {

    boolean transferFromAccountToAccount(String sender, String receiver, int total);
}
