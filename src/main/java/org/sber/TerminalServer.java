package org.sber;

import lombok.Data;
import org.sber.exception.NotEnoughMoneyInTheAccountException;

@Data
public class TerminalServer {

    private int moneyInTheAccount = 500;

    public void doTransferAccountToAccount(String sender, String receiver, int total){
        if (total > moneyInTheAccount) {
            throw new NotEnoughMoneyInTheAccountException("Сумма денег на вашем счете меньше, чем " + total + ". Пополните баланс");
        } else {
            System.out.printf("Выполнен перевод между вашими счетами. Со счета %s на счет %s на сумму %d \n", sender, receiver, total);
        }
    }


}
