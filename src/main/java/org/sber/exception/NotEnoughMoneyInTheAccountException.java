package org.sber.exception;

public class NotEnoughMoneyInTheAccountException extends RuntimeException{

    public NotEnoughMoneyInTheAccountException(String message) {
        super(message);
    }


}
