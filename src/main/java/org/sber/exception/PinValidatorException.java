package org.sber.exception;

public class PinValidatorException extends RuntimeException {
    public PinValidatorException(String message) {
        super(message);
    }
}
