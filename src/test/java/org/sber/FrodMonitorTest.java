package org.sber;

import org.sber.exception.TransferAmountValidationException;
import org.sber.exception.TranslationSecurityCheckException;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class FrodMonitorTest {


    @Test(expectedExceptions = TranslationSecurityCheckException.class)
    public void translationSecurityCheckUnsuccessTest() {
        FrodMonitor frodMonitor = new FrodMonitor();
        for (int i = 0; i < 20; i++) {
            frodMonitor.translationSecurityCheck();
        }
    }

    @Test
    public void translationSecurityCheckSuccessTest() {
        FrodMonitor frodMonitor = new FrodMonitor();
        for (int i = 0; i < 10; i++) {
            frodMonitor.translationSecurityCheck();
        }
    }

    @Test
    public void transferAmountValidationTest(){
        FrodMonitor frodMonitor = new FrodMonitor();

        assertThrows(TransferAmountValidationException.class, () ->{
            frodMonitor.transferAmountValidation(111);
        });
    }
}