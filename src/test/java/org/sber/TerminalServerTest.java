package org.sber;

import org.sber.exception.NotEnoughMoneyInTheAccountException;
import org.sber.exception.TransferAmountValidationException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class TerminalServerTest {

    TerminalServer terminalServer;

    @BeforeClass
    public void setUp(){
        terminalServer = new TerminalServer();
    }

    @Test(expectedExceptions = NotEnoughMoneyInTheAccountException.class)
    public void testDoTransferAccountToAccountUnsuccess() {
        terminalServer.doTransferAccountToAccount("4234234", "234234", 1000);
    }


    @Test
    public void testDoTransferAccountToAccountSuccess() {
        terminalServer.doTransferAccountToAccount("4234234", "234234", 400);
    }


}
